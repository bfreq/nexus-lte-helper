package com.example.nexusltehelper;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onResume() {
        // Logic goes here so that every time the click on the app it works.

        // Putting code to clipboard
        String code = "*#*#4636#*#*";
        String description = "Code for LTE";
        ClipData clip = ClipData.newPlainText(description, code);
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(clip);

        // Letting the user know what to do
        Toast.makeText(this, "Long press and paste into dialer", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(Intent.ACTION_DIAL);
        startActivity(intent);
        super.onResume();
    }
}
